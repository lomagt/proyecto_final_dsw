<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\MessagesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'language'], function () {
    Route::get('/', function () {
        return view('start');
    });


    Auth::routes(['verify' => 'true']);
    Route::group(['middleware' => 'verified'], function () {

        Route::group(['prefix' => 'messages'], function () {
            //Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
            Route::get('/', [App\Http\Controllers\MessagesController::class, 'index'])->name('messages');
            Route::get('create', [App\Http\Controllers\MessagesController::class, 'create'])->name('messages.create');
            Route::post('/', [App\Http\Controllers\MessagesController::class, 'store'])->name('messages.store');
            Route::get('{id}', [App\Http\Controllers\MessagesController::class, 'show'])->name('messages.show');
            Route::put('{id}', [App\Http\Controllers\MessagesController::class, 'update'])->name('messages.update');
        });

        // Rutas a verificar
        Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

        Route::get('/apuestas/{idPartido}', [App\Http\Controllers\ApuestaController::class, 'index'])->name('apuestas')->middleware('auth');
        Route::post('/apuestas/{idPartido}', [App\Http\Controllers\ApuestaController::class, 'store'])->middleware('auth');
        Route::get('/delete-apuseta/{user_id}/partido/{partido_id}', [App\Http\Controllers\ApuestaController::class, 'destroy'])->name('destroy-apuesta')->middleware('auth');

        Route::get('/show-mail/{showMail}', [App\Http\Controllers\HomeController::class, 'index'])->name('show_mail')->middleware('auth');
        Route::post('/send-mail', [App\Http\Controllers\HomeController::class, 'send'])->name('send_mail')->middleware('auth');

        Route::group(['middleware' => 'admin'], function () {

            Route::get('/management', [App\Http\Controllers\PartidoController::class, 'index'])->name('management')->middleware('auth')->middleware('admin');
            Route::get('/management/{show}', [App\Http\Controllers\PartidoController::class, 'index'])->name('managementShow')->middleware('auth');
            Route::get('/management2/{showMatch}', [App\Http\Controllers\PartidoController::class, 'index'])->name('managementShowAddMatch')->middleware('auth');
            Route::get('/management3/{showResult}/result/{partId}', [App\Http\Controllers\PartidoController::class, 'index'])->name('managementShowAddResult')->middleware('auth');
            Route::post('/management', [App\Http\Controllers\PartidoController::class, 'store'])->name('team_create')->middleware('auth');
            Route::post('/management-create', [App\Http\Controllers\PartidoController::class, 'create'])->middleware('auth');
            Route::post('/management-update/{partido}', [App\Http\Controllers\PartidoController::class, 'update'])->middleware('auth');
            Route::get('/delete-match/{partido}', [App\Http\Controllers\PartidoController::class, 'destroy'])->name('delete_match')->middleware('auth');
        });
    });
});
