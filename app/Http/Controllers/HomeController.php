<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Queries\PartidoQueries;
use App\Http\Requests\SendMailForm;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $search = $request->search;

        $showAddMail = false;
        if ($request->showMail) {
            $showAddMail = true;
        } else {
            $showAddMail = false;
        }

        $matchWithTeams = (new PartidoQueries)->getMatchWithTeams($search);

        return view('home', compact('matchWithTeams', 'showAddMail', 'search'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function send(SendMailForm $request)
    {
        $details = [

            'email' => Auth::User()->email,

            'title' => $request->asunto,
    
            'body' => $request->body,
    
        ];
    
       
    
        \Mail::to('your_receiver_email@gmail.com')->send(new \App\Mail\MyTestMail($details));

        return redirect()->route('home')->with('success', 'Email send successfully');
    }
}
