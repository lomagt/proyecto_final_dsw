<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Queries\PartidoQueries;
use App\Queries\ApuestaQueries;
use App\Http\Requests\ApuestaForm;
use Illuminate\Support\Facades\Auth;
use App\MyService\Facades\Price;
use App\Models\Partido;

class ApuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     * Desde la función index visualizaremos la vista de apuestas a la cual le mandamos 
     * un array con los apostantes sus nombres pronostico realizado y el valor de la 
     * apuesta realizada.
     * también contiene otro array con los acertantes del resultado del partido y el valor
     * correspondiente de la apuesta a recibir
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $idPartido = $request->idPartido;
        $apuestasWithUserAndMatches = (new PartidoQueries)->getUserWithMatchAndTeams($idPartido);
        $equipos = (new PartidoQueries)->getTeamsByMacthWithVisitor($idPartido);
        $equipoLocal = $equipos->where('visitor', false);
        $equipoVisitor = $equipos->where('visitor', true);

        $winnerAndMoney = (new ApuestaQueries)->winnersAndMoney($idPartido);
        $totalBets = (new ApuestaQueries)->totalbets($idPartido);
        if(count($totalBets) != 0){
            Price::price($totalBets[0]->total_bets, $winnerAndMoney);
        }
        return view('apuestas.apuestas', compact('apuestasWithUserAndMatches', 'winnerAndMoney','totalBets', 'equipoVisitor', 'equipoLocal'));
    }

    /**
     * Store a newly created resource in storage.
     * Desde store se gestiona la actualización y creación de apuestas
     * 
     * @param  \Illuminate\Http\ApuestaForm  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApuestaForm $request)
    {
        
        if(Price::matchClose($request->idPartido) != 1){
            $updateOrCreateApuesta = (new ApuestaQueries)->createBetorUpdate(Auth::user()->id, $request->idPartido, $request->all());
            //dd($updateOrCreateApuesta);
            return back();
        }else{
            return back()->with('info', 'Match close, Bets not allowed');
        }
    }

    /**
     * Store a newly created resource in storage.
     * Desde destroy se gestiona las peticiones de borrado de realizadas 
     * desde la vista de apuestas
     * 
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(Price::matchClose($request->partido_id) != 1){
            (new ApuestaQueries)->deleteBet($request->user_id,$request->partido_id);
            return back();
        }else{
            return back()->with('info', 'Match close, Bets disabled');
        }
    }
}
