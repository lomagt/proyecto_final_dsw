<?php

namespace App\Queries;

use App\Models\Apuesta;
use Illuminate\Support\Facades\DB;

class ApuestaQueries
{
    /**
     * Función de creado y actualizacion de una apuesta realizada por un usuario en un partido
     * incluye una query a base de datos en la que se comprueba que si exite un partido,
     * en caso afirmativo lo actualiza en caso contrario se envia una solicitud a la base 
     * de datos para crear una apuesta nueva.
     * 
     * @param $idUser
     * @param $idPartido
     * @param $request array
     */

    public function createBetorUpdate($idUser, $idPartido, $request)
    {
        //dd($idPartido);
        $apuesta = Apuesta::where([['user_id', $idUser], ['partido_id', $idPartido]])
            ->update(['result_visitor' => $request['result_visitor'], 'result_local' => $request['result_local'], 'value_bet' => $request['value_bet']]);

        if (!$apuesta) {
            $apuesta = Apuesta::create(['user_id' => $idUser, 'partido_id' => $idPartido, 'result_visitor' => $request['result_visitor'], 'result_local' => $request['result_local'], 'value_bet' => $request['value_bet']]);
        }

        return true;
    }
    /**
     * Desde la función deletebet se borrará una apuesta de un usuario en un partido determinado
     */
    public function deleteBet($idUser, $idPartido)
    {
        $resp = Apuesta::where([['user_id', $idUser], ['partido_id', $idPartido]])->delete();
        if ($resp) {
            return true;
        }

        return false;
    }
    /**
     * Desde la función winnersAndMoney se obtienen los ganadores de una apuesta y las cantidades apostadas
     * por los apostantes
     */
    public function winnersAndMoney($idPartido)
    {
        $betsByMatch = DB::table('apuestas')
            ->join('partidos', function ($join) use ($idPartido) {
                $join->on('apuestas.partido_id', '=', 'partidos.id')
                    ->where('partidos.id', '=', $idPartido);
            })
            ->get();
        $UserByBets = DB::Table('users')
            ->join('apuestas', 'users.id', '=', 'apuestas.user_id')
            ->where('apuestas.partido_id', '=', $idPartido)
            ->select('users.name', 'apuestas.*')
            ->latest('updated_at')
            ->get();

        $winnerAndMoney = [];
        foreach ($betsByMatch as $key => $value) {

            foreach ($UserByBets as $value2) {

                if ($value->user_id == $value2->user_id) {

                    if ($value->result_visitor == $value->result_eq_visitor && $value->result_local == $value->result_eq_local) {

                        $winnerAndMoney[$key] = $value2;
                    }
                }
            }
        }

        //dd($winnerAndMoney);
        return $winnerAndMoney;
    }
/**
 * Desde la función totalbets se obtiene la suma total de las apuestas
 * realizadas por los apostantes
 */
    public function totalbets($idPartido)
    {
        $totalBets = DB::Table('apuestas')
            ->where('apuestas.partido_id', $idPartido)
            ->select('apuestas.partido_id', DB::raw('SUM(value_bet) as total_bets'))
            ->groupBy('apuestas.partido_id')
            ->get();

        return $totalBets;
    }
}
