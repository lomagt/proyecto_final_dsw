@extends('layouts.app')

@section('content')
<div class="global">
    <div class="container-fluid">

        <div class="row d-flex justify-content-center">
            @include('flash-message')
            <div class="col-11 col-sm-10 col-md-7 col-lg-7">
                @include('apuestas.partials.bets_users')
                @if(count($apuestasWithUserAndMatches) === 0)
                <h4>{{__('There are no bettors')}}</h4>
                @else
                {{$apuestasWithUserAndMatches->appends($_GET)->links()}}
                @endif


            </div>
            <div class="col-11 col-sm-10 col-md-4 col-lg-4">
                @include('apuestas.partials.bets_add')

            </div>
        </div>
    </div>
</div>
@include('footer.footer')
@endsection