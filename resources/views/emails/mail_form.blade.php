@php
if($showAddMail){
@endphp
<div class="show-add-team">
    <div class="container-team">
        <form action="/send-mail" method="post" class="border border-1 shadow-lg rounded-2 p-3">
            {{ csrf_field() }}
            <div class="d-flex justify-content-between">
                <h4><i class="fa-solid fa-futbol text-success"></i> {{__('Send a mail to the admin')}}</h4>
                <a href="{{route('home')}}"><i class="admin-icon fa-solid fa-circle-xmark fa-2x text-secondary"></i></a>
            </div>
            <hr>
            <div class="mb-3">
                <label for="clubinput" class="form-label">{{__('theme')}}</label>
                <input type="text" class="form-control @error('asunto') is-invalid @else is-valid @enderror" id="clubinput" placeholder="{{__('Theme')}}" name="asunto">
            </div>
            @error('asunto')
            <span class="text-danger">{{ $message }}</span>
            @enderror
            <div class="mb-3">
                <label for="coachinput" class="form-label">{{__('Body')}}</label>
                <textarea type="text" class="form-control @error('body') is-invalid @else is-valid @enderror" id="coachinput" placeholder="{{__('Your description here')}}" name="body">
                </textarea>    
            </div>
            @error('body')
            <span class="text-danger">{{ $message }}</span>
            @enderror
            <div class="d-flex justify-content-end">
                <button type="submit" class="btn btn-outline-dark">{{__('Send Mail')}}</button>
            </div>
        </form>
    </div>
</div>
@php
}
@endphp