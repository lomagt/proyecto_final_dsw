@extends('layouts.app')

@section('content')
<div class="global">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif
            </div>
        </div>

    </div>
    @include('flash-message')
    @include('home.partials.matches')
    @include('emails.mail_form')
</div>
@include('footer.footer')
@endsection