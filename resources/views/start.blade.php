@extends('layouts.app')

@section('content')
<div class="global">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-9">
                <div class="fondo-principal border border-3 d-flex justify-content-center">
                    <h2 class="titulo text-white fs-1">{{__('BestBar Web')}}</h2>
                </div>
            </div>
        </div>
    </div>
</div>
@include('footer.footer')
@endsection