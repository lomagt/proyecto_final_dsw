@extends('layouts.app')

@section('content')
<div class="container-fluid d-flex justify-content-center">

    <form action="{{ route('messages.store') }}" method="post" class="row d-flex justify-content-center">
        {{ csrf_field() }}
        
        <div class="col-11 col-sm-9 col-md-7 col-lg-7 p-3 border border-1 rounded-2 shadow-lg">
            <h1>{{__('Create a new message')}}</h1>
            <hr>
            <!-- Subject Form Input -->
            <div class="form-group mb-3">
                <label class="control-label">{{__('Subject')}}</label>
                <input type="text" class="form-control" name="subject" placeholder="Subject" value="{{ old('subject') }}">
            </div>

            <!-- Message Form Input -->
            <div class="form-group mb-3">
                <label class="control-label">{{__('Message')}}</label>
                <textarea name="message" class="form-control">{{ old('message') }}</textarea>
            </div>

            @if($users->count() > 0)
            <div class="checkbox mx-2 mb-2">
                @foreach($users as $user)
                <label title="{{ $user->name }}"><input type="checkbox" name="recipients[]" value="{{ $user->id }}"> {!!$user->name!!}</label>
                @endforeach
            </div>
            @endif

            <!-- Submit Form Input -->
            <div class="d-flex justify-content-end align-content-end">
                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
            </div>
        </div>
    </form>
</div>
@stop