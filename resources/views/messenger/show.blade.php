@extends('layouts.app')

@section('content')
<div class="container-fluid d-flex justify-content-center">
    <div class="col-11 col-sm-10 col-md-8 col-lg-6">
        <h1 class="text-decoration-none text-info text-capitalize">{{ $thread->subject }}</h1>
        <hr>
        @each('messenger.partials.messages', $thread->messages, 'message')

        @include('messenger.partials.form-message')
    </div>
</div>
@stop
