<div class="d-flex justify-content-start p-2 border border-1 rounded-2 mb-2">
    <a class="" href="#">
        <img src="//www.gravatar.com/avatar/{{ md5($message->user->email) }} ?s=64"
             alt="{{ $message->user->name }}" class="rounded-circle border border-1">
    </a>
    <div class="media-body mx-3 mb-2">
        <h5 class="media-heading">{{ $message->user->name }}</h5>
        <p>{{ $message->body }}</p>
        <div class="text-muted">
            <small>Posted {{ $message->created_at->diffForHumans() }}</small>
        </div>
    </div>
</div>