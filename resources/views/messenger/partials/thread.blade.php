<?php $class = $thread->isUnread(Auth::id()) ? 'alert-info' : ''; ?>
<div class="row d-flex justify-content-center">
    <div class="col-10">
        <div class="d-flex justify-content-start">
            <div class="{{ $class }}">
                <h4 class="media-heading">
                    <a class="text-decoration-none text-info fs-3 text-capitalize" href="{{ route('messages.show', $thread->id) }}">{{ $thread->subject }}</a>
                    ({{ $thread->userUnreadMessagesCount(Auth::id()) }} unread)</h4>
                    <hr>
                <p>
                    {{ $thread->latestMessage->body }}
                </p>
                <p>
                    <small><strong>Creator:</strong> {{ $thread->creator()->name }}</small>
                </p>
                <p>
                    <small><strong>Participants:</strong> {{ $thread->participantsString(Auth::id()) }}</small>
                </p>
            </div>
        </div>
    </div>
</div>